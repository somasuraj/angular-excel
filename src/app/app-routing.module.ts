import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExcelComponent } from './excel/excel.component';
import { ArrayComponent } from './array/array.component';


const routes: Routes = [
  { path: '', component: ExcelComponent, pathMatch: 'full' },
  { path : 'array', component: ArrayComponent, pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
