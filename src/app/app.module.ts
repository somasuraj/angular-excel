import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ExcelComponent } from './excel/excel.component';
import { IgxExcelModule } from 'igniteui-angular-excel';
import { ArrayComponent } from './array/array.component';

@NgModule({
  declarations: [
    AppComponent,
    ExcelComponent,
    ArrayComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    IgxExcelModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
