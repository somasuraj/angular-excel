import { Component, OnInit } from '@angular/core';
import { Workbook, IWorkbookFont, DataValidationRule, Worksheet, AnyValueDataValidationRule, OneConstraintDataValidationRule, TwoConstraintDataValidationRule, TwoConstraintDataValidationOperator, DataValidationErrorStyle } from "igniteui-angular-excel";
import { WorkbookSaveOptions } from "igniteui-angular-excel";
import { WorkbookFormat } from "igniteui-angular-excel";
import { ExcelUtility } from "../excel-utility";
import { arrayShallowClone } from 'igniteui-angular-core';

@Component({
  selector: 'app-excel',
  templateUrl: './excel.component.html',
  styleUrls: ['./excel.component.css']
})
export class ExcelComponent implements OnInit {

  headerData: Map<string, string>;
  fileToUpload: File = null;
  sampleSheetData = {};
  uploadedWorkbookName: string = null;
  uploadedWorkbook: Workbook = null;
  uploadedWoeksheet : Worksheet = null;
  temp: string = null;
  loading: boolean;

  constructor() {

    this.headerData = new Map();

    this.headerData.set("name", "suraj");
    this.headerData.set("name", "himanshu");

    // this.sampleSheetData['name'] = "Suraj";
    // this.sampleSheetData['surname'] = "Soma";
  }

  ngOnInit(): void {
  }

  download() {
    this.uploadedWorkbook.worksheets(0).filterSettings.setRegion("Sheet1!A1:A6")
    this.uploadedWorkbook.worksheets(0).filterSettings.applyFixedValuesFilter;


    const numericRule = new TwoConstraintDataValidationRule();
    numericRule.validationOperator = TwoConstraintDataValidationOperator.Between;
    numericRule.setLowerConstraint(0);
    numericRule.setUpperConstraint(100);
    numericRule.inputMessageTitle = "Only Numbers are allowed";
    numericRule.inputMessageDescription = "Numbers betwwen 0 to 100 are allowed";
    numericRule.errorMessageTitle = "requirement not met";
    numericRule.errorMessageDescription = "Number must be between 1 and 100.";
    numericRule.errorStyle = DataValidationErrorStyle.Information;
  

    this.uploadedWorkbook.worksheets(0).rows(1).cells(0).dataValidationRule = numericRule;

    
    ExcelUtility.save(this.uploadedWorkbook, this.uploadedWorkbookName);
  }

  handleFileInput(files: FileList) {
    this.loading = true;
    this.fileToUpload = files.item(0);
    const workbook = ExcelUtility.load(this.fileToUpload);
    this.uploadedWorkbookName = this.fileToUpload.name.replace(".xlsx", "");
    workbook.then((wb) => {
      this.uploadedWorkbook = wb;
      this.uploadedWoeksheet = this.uploadedWorkbook.worksheets(0);
      console.log(this.uploadedWoeksheet);
      this.sampleSheetData = {};
      for(let i=0; i<6; i++) {
        this.sampleSheetData[this.uploadedWoeksheet.rows(0).cells(i).value] = this.uploadedWoeksheet.rows(1).cells(i).value
      }
      this.loading = false;
    }
    );
  }

}
