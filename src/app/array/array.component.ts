import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-array',
  templateUrl: './array.component.html',
  styleUrls: ['./array.component.css']
})
export class ArrayComponent implements OnInit {

  myObject: any = {};

  constructor() { }

  typeOf(value) {
    return typeof value;
  }

  ngOnInit(): void {
    this.myObject['key_1'] = 'value_1';
    this.myObject['key_2'] = 'value_2';
    this.myObject['key_3'] = 'value_3';
    this.myObject['key_4'] = ['value_4_1', 'value_4_2', 'value_4_3'];
    this.myObject['key_5'] = 'value_5';

    for ( let item in this.myObject) {
      console.log(this.myObject[item]);
    }
  }

}
